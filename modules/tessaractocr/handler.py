#!/usr/bin/env python3
from PIL import Image
import pytesseract
import numpy as np

def handle(req):
    filename = req["img"]
    img1 = np.array(Image.open(filename))
    text = pytesseract.image_to_string(img1, config=f"--psm {req['psm']}")
    print(text)
    print("Hi jessica!")
    return { 'ocr_text': text }

if __name__ == "__main__":
    handle({
        "img": "./test-image.jpg",
        "psm": 3,
    })
